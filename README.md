# SkillBox final work
## Test Service Deployment 
### To deploy a service (application), you must perform the following steps:
  1. Make sure that the runner with tag 'build' (docker runner) is registered for our project.
  2. Check that variables are set in the project settings - CI/CD section (used to name an docker image):
   ```     
- image_app_name ($CI_PROJECT_NAME)
- image_app_tag ($CI_COMMIT_REF_SLUG)
- branch_for_test_deploy (uat)  
   ```     
  3. When committing to any branch, an application test will be launched in the pipeline
  4. When committing to the branch 'uat', the test will be launched, the application image will be built and the application will be deployed to the test environment. 
  #### Important: 'uat' branch should be protected!

To check that everything went well, navigate in your browser to the address:
http://testapp.sigmanet.website
   

## App description


## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением без docker:  
1. Установить golang 1.16
2. Установить зависимости:

   ```shell
   go mod download
   ```

3. Запустить тесты:
   ```shell
   go test -v ./...
   ```
4. Собрать приложение:
   ```
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Запустить его:
   ```shell
   ./app
   ```

## Как работать с приложением в docker:  
1. Установить docker
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p8080:8080 skillbox/app
   ```
